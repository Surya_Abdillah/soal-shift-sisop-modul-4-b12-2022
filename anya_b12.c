#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

void encode(char *str)
{
    char *p = str;
    while (*p != '\0')
    {
        if (*p == '.')
            break;

        if (*p >= 'A' && *p <= 'Z')
            *p = 'Z' + 'A' - *p;
        if (*p >= 'a' && *p <= 'z')
            *p = 'z' + 'a' - *p;

        p++;
    }
    return str;
};

void decode(char *str)
{
    char *p = str;
    while (*p != '\0')
    {
        if (*p == '.')
            break;

        if (*p >= 'A' && *p <= 'Z')
            *p = 'Z' + 'A' + *p;
        if (*p >= 'a' && *p <= 'z')
            *p = 'a' + (*p - 'a' + 13) % 26;

        p++;
    }
}

static int
xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    fd = open(path, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    res = rename(from, to);

    if (strncmp(&from[strlen(from) - strlen("Animeku_")], "Animeku_", strlen("Animeku_")) == 0)
    {
        printf("Decode\n");

        DIR *d;
        struct dirent *dir;
        d = opendir(to);
        if (d)
        {
            while ((dir = readdir(d)) != NULL)
            {
                char new_name[100], current_file[100], new_file[100];

                strcpy(new_name, dir->d_name);

                decode(new_name);

                sprintf(current_file, "%s/%s", to, dir->d_name);
                sprintf(new_file, "%s/%s", to, new_name);

                rename(dir->d_name, new_name);
                printf("decoded: %s\n", new_name);
            }
            closedir(d);
        }
    }

    if (strncmp(&to[strlen(to) - strlen("Animeku_")], "Animeku_", strlen("Animeku_")) == 0)
    {
        printf("Encode\n");

        DIR *d;
        struct dirent *dir;
        d = opendir(to);
        if (d)
        {
            while ((dir = readdir(d)) != NULL)
            {
                char new_name[100], current_file[100], new_file[100];

                strcpy(new_name, dir->d_name);

                encode(new_name);

                sprintf(current_file, "%s/%s", from, dir->d_name);
                sprintf(new_file, "%s/%s", from, new_name);

                printf("Current: %s\n", current_file);
                printf("New: %s\n", new_file);

                if (rename(dir->d_name, new_name) == 0)
                    printf("renamed: %s\n", new_name);

                printf("encoded: %s\n", new_name);
            }
            closedir(d);
        }
    }

    if (res == -1)
        return -errno;

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);
    printf("Fuse is running....\n");
    return fuse_main(argc, argv, &xmp_oper, NULL);
}